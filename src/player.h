/* tuneclip
*    Copyright (C) 2022  Lucas Santos
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <mpv/client.h>
#include <pthread.h>
#include "main.h"
#include "czerolib/czerolib.h"

#define MAX_VOLUME 100

void * start_player(void * data);
void play(char filename[]);
void toggle_mute();
void toggle_pause();
void step_volume(int step);
void seek(int step);

#endif
