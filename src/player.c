/* tuneclip
*    Copyright (C) 2022  Lucas Santos
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "player.h"

mpv_handle *ctx;
queue * event_q;
pthread_mutex_t queue_mutex = PTHREAD_MUTEX_INITIALIZER;

extern struct display * dp;
extern pthread_mutex_t dp_mutex;
extern pthread_mutex_t locale_mutex;

extern pthread_mutex_t player_init_mutex;
extern pthread_cond_t player_init_cond;
extern bool player_init;

bool event_q_initialized = false;
bool item_added = false;
bool playing = false;
pthread_cond_t event_q_created_cond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t event_q_init_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t item_added_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t item_added_cond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t playing_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t playing_cond = PTHREAD_COND_INITIALIZER;

void terminate_player() {
    mpv_terminate_destroy(ctx);
}

/* mpv function for error checking */
static inline void check_error(int status) {
    if (status < 0) {
        error_exit(mpv_error_string(status));
    }
}

void * event_queuer() {
    event_q = init_queue(8, 4);
    pthread_mutex_lock(&event_q_init_mutex);
    event_q_initialized = true;
    pthread_cond_signal(&event_q_created_cond);
    pthread_mutex_unlock(&event_q_init_mutex);
    mpv_event * new_event;
    mpv_event * event_tosave;

    while (1) {
        event_tosave = malloc(sizeof(mpv_event));
        new_event = mpv_wait_event(ctx, -1); // waits for events indefinetely
        *event_tosave = *new_event;

        pthread_mutex_lock(&queue_mutex);
        enqueue(event_q, event_tosave);
        pthread_mutex_unlock(&queue_mutex);

        pthread_mutex_lock(&item_added_mutex);
        item_added = true;
        pthread_cond_signal(&item_added_cond);
        pthread_mutex_unlock(&item_added_mutex);
    }
}

bool event_dequeue(mpv_event * event) {
    while (1) {
        pthread_mutex_lock(&queue_mutex);
        mpv_event * event_ptr = dequeue(event_q);
        pthread_mutex_unlock(&queue_mutex);
        if (event_ptr != NULL) {
            *event = *event_ptr;
            free(event_ptr);
            return true;
        } else { // If event queue is empty
            pthread_mutex_lock(&item_added_mutex);
            while (item_added == false) {
                pthread_cond_wait(&item_added_cond, &item_added_mutex);
            }
            item_added = false;
            pthread_mutex_unlock(&item_added_mutex);
        }
    }
}

void play(char filename[]) {
    const char *cmd[] = {"loadfile", filename, NULL};
    mpv_command(ctx, cmd);

    pthread_mutex_lock(&playing_mutex);
    playing = true;
    while (playing) {
        pthread_cond_wait(&playing_cond, &playing_mutex);
    }
    pthread_mutex_unlock(&playing_mutex);
}

void step_volume(int step) {
    if (step > 0) {
        if (dp->volume + step >= MAX_VOLUME)
            dp->volume = MAX_VOLUME;
        else
            dp->volume += step;
    } else {
        if (dp->volume + step > 0)
            dp->volume += step;
        else
            dp->volume = 0;
    }

    if (!dp->is_muted) {
        char * buff = malloc(6 * sizeof(char));

        sprintf(buff, "%d", dp->volume);
        check_error(mpv_set_option_string(ctx, "volume", buff));

        free(buff);
    }
}

void toggle_mute() {
    char * buff = malloc(6 * sizeof(char));

    if (dp->is_muted) {
        sprintf(buff, "%d", dp->volume);
        check_error(mpv_set_option_string(ctx, "volume", buff));
        dp->is_muted = false;
    } else {
        sprintf(buff, "%d", 0);
        check_error(mpv_set_option_string(ctx, "volume", buff));
        dp->is_muted = true;
    }

    free(buff);
}

void toggle_pause() {
    mpv_command_string(ctx, "cycle pause");

    if (dp->is_paused) {
        dp->is_paused = false;
    } else {
        dp->is_paused = true;
    }
}

void seek(int step) {
    char * buff = malloc(16 * sizeof(char));
    sprintf(buff, "seek %d", step);
    mpv_command_string(ctx, buff);

    /* to improve responsiveness */
    pthread_mutex_lock(&dp_mutex);
    dp->position_secs += step;
    pthread_mutex_unlock(&dp_mutex);

    free(buff);
}

void * timer() {
    while (1) {
        int64_t pos;

        mpv_get_property(ctx, "time-pos", MPV_FORMAT_INT64, &pos);

        pthread_mutex_lock(&dp_mutex);
        dp->position_secs = pos;
        pthread_mutex_unlock(&dp_mutex);

        usleep(100000);
    }
}

void * start_player(void * data) {

    pthread_t event_queuer_thread;
    pthread_t timer_thread;

    /* create and initialize mpv */
    pthread_mutex_lock(&locale_mutex);
    ctx = mpv_create();
    if (!ctx) {
        error_exit("mpv: failed creating context!\n");
    }
    pthread_mutex_unlock(&locale_mutex);
    /* set mpv options */
    check_error(mpv_set_option_string(ctx, "audio-display", "no"));
    check_error(mpv_set_option_string(ctx, "af", "loudnorm=I=-15"));
    check_error(mpv_initialize(ctx));

    {
        int64_t volume = dp->volume;
        check_error(mpv_set_option(ctx, "volume", MPV_FORMAT_INT64, &volume));
    }

    /* initialize event queue handler */
    pthread_create(&event_queuer_thread, NULL, event_queuer, NULL);

    pthread_mutex_lock(&event_q_init_mutex);
    while (event_q_initialized == false) {
        pthread_cond_wait(&event_q_created_cond, &event_q_init_mutex);
    }
    pthread_mutex_unlock(&event_q_init_mutex);

    mpv_event event;

    pthread_mutex_lock(&player_init_mutex);
    player_init = true;
    pthread_cond_signal(&player_init_cond);
    pthread_mutex_unlock(&player_init_mutex);


    while (1) {
        event_dequeue(&event); // blocking call

        switch (event.event_id) {
            case MPV_EVENT_FILE_LOADED:
            {

                pthread_create(&timer_thread, NULL, timer, NULL);

                int64_t length;
                mpv_get_property(ctx, "duration", MPV_FORMAT_INT64, &length);

                int64_t position_secs;
                mpv_get_property(ctx, "time-pos", MPV_FORMAT_INT64, &position_secs);

                char * name = mpv_get_property_string(ctx, "media-title");

                wchar_t * wname = malloc(CHAR_LIMIT_NAME * sizeof(wchar_t));
                mbstowcs(wname, name, CHAR_LIMIT_NAME);
                mpv_free(name);

                pthread_mutex_lock(&dp_mutex);

                /* store track title */
                wcscpy(dp->name, wname);

                /* store track length */
                dp->length_secs = (int) length;

                /* store first track position */
                dp->position_secs = (int) position_secs;

                pthread_mutex_unlock(&dp_mutex);

                free(wname);
                break;
            }
            case MPV_EVENT_END_FILE:
                pthread_cancel(timer_thread);
                pthread_mutex_lock(&dp_mutex);
                wcscpy(dp->name, L"Nothing playing");
                dp->length_secs = 0;
                dp->position_secs = 0;
                pthread_mutex_unlock(&dp_mutex);

                pthread_mutex_lock(&playing_mutex);
                playing = false;
                pthread_cond_signal(&playing_cond);
                pthread_mutex_unlock(&playing_mutex);

                break;
            default:
                break;
        }
    }

    return NULL;
}




