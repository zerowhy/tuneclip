/* tuneclip
*    Copyright (C) 2022  Lucas Santos
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __MAIN_H__
#define __MAIN_H__

#define _XOPEN_SOURCE 700

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <wchar.h>
#include <locale.h>
#include <pthread.h>
#include <stdbool.h>
#include <time.h>
#include <linux/limits.h>
#include <ftw.h>
#include <errno.h>

#define CHAR_LIMIT_NAME 128
#define INIT_VOLUME 50

#define FILE_TYPES {"mp3", "flac", "ogg", "m4a", "wav"}
#define FILE_TYPES_COUNT 5
#define MAX_FILES 256

struct display {
    wchar_t * name;
    int length_secs;
    int position_secs;

    int volume;
    bool is_muted;
    bool is_loop;
    bool is_paused;

};

void error_exit(const char * msg);

void end_exit();

#endif
