/* tuneclip
*    Copyright (C) 2022  Lucas Santos
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __CZEROLIB_QUEUE_H__
#define __CZEROLIB_QUEUE_H__

#include <stdio.h>
#include <stdlib.h>
#include "../common/common.h"

typedef struct queue {
    void ** values;
    int num_values;
    int mem_allocated;
    int mem_allocate_step;
} queue;

queue * init_queue(int initial_size, int mem_alloc_step);
void * enqueue(queue * queue, void * data);
void * dequeue(queue * queue);
void show(queue * queue);
void end_queue(queue * q);

#endif
