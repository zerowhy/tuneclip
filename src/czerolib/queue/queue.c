/* tuneclip
*    Copyright (C) 2022  Lucas Santos
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "queue.h"

queue * init_queue(int initial_size, int mem_allocate_step) {
    queue * q = malloc(sizeof(queue));
    check_malloc(q);
    q->mem_allocate_step = mem_allocate_step;
    q->num_values = 0;
    q->values = malloc(initial_size * sizeof(void *));
    check_malloc(q->values);
    q->mem_allocated = initial_size * sizeof(void *);

    return q;

}

void * enqueue(queue * q, void * data) {
    if (q->num_values < q->mem_allocated/sizeof(void *)) {
        q->values[q->num_values] = data;
        q->num_values += 1;
        return data;
    } else {
        /* allocate more memory */
        int new_size = q->mem_allocated/sizeof(void *) + q->mem_allocate_step;
        void ** new_queue = malloc(new_size * sizeof(void *));
        check_malloc(new_queue);
        for (int i = 0; i < q->mem_allocated/sizeof(void *); i++)
            new_queue[i] = q->values[i];
        void ** old_queue = q->values;
        q->values = new_queue;
        free(old_queue);
        q->mem_allocated += q->mem_allocate_step*sizeof(void *);

        q->values[q->num_values] = data;
        q->num_values += 1;
        return data;
    }
}

void * dequeue(queue * q) {
    if (q->num_values > 0) {
        void * result = q->values[0];
        for (int i = 0; i < q->num_values-1; i++) {
            q->values[i] = q->values[i+1];
        }
        q->num_values -= 1;
        return result;
    } else {
        return NULL;
    }
}

void end_queue(queue * q) {
    free(q->values);
    free(q);
}

void show(queue * q) {
    int * data;
    for (int i = 0; i < q->num_values; i++) {
        data = (int *) q->values[i];
        printf("%d: %d\n", i, *data);
    }
}

