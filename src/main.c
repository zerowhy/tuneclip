/* tuneclip
*    Copyright (C) 2022  Lucas Santos
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "main.h"
#include "gui_curses.h"
#include "player.h"

extern mpv_handle *ctx;
extern bool playing;

struct display * dp;

pthread_mutex_t dp_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t locale_mutex = PTHREAD_MUTEX_INITIALIZER;

pthread_mutex_t player_init_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t player_init_cond = PTHREAD_COND_INITIALIZER;
bool player_init = false;
pthread_t interface_thread, player_thread;


char file_types[FILE_TYPES_COUNT][16] = FILE_TYPES;
char ** file_list;
int file_count = 0;

struct display * init_display_struct() {
    pthread_mutex_lock(&dp_mutex);
    struct display * dp = malloc(sizeof(struct display));
    dp->name = malloc(CHAR_LIMIT_NAME * sizeof(wchar_t));
    wcscpy(dp->name, L"Nothing playing");
    dp->length_secs = 0;
    dp->position_secs = 0;
    dp->volume = INIT_VOLUME;
    dp->is_muted = false;
    dp->is_loop = false;
    dp->is_paused = false;
    pthread_mutex_unlock(&dp_mutex);

    return dp;
}

void error_exit(const char * msg) {
    endwin();
    printf("An error occurred: %s\n", msg);
    exit(1);
}

void end_exit() {
    endwin();
    printf("Program terminated correctly!\n");
    exit(1);

}

void shuffle(void ** array, int n_elements) {
    srand(time(NULL));
    void * buff;
    int rnum;

    for (int i = 0; i < n_elements; i++) {
        rnum = abs(rand()) % n_elements;

        buff = array[i];
        array[i] = array[rnum];
        array[rnum] = buff;
    }
}

/* function called with nftw to read file paths */
int store_file(const char *fpath, const struct stat *sb, int typeflag) {
    if (typeflag == FTW_F) {
        int len;

        len = strlen(fpath); // start from second last letter (ignore last)
        for (int i = len-2; i > 0; i--) {
            if (fpath[i] == '.') {
                char *buff = calloc(len-i, sizeof(char));
                char ch[2];

                for (int j = i+1; j < len; j++) {
                    ch[0] = fpath[j];
                    ch[1] = '\0';
                    strcat(buff, ch);
                }
                for (int k = 0; k < FILE_TYPES_COUNT; k++) {
                    if (!strcmp(buff, file_types[k])) {
                        strcpy(file_list[file_count], fpath);
                        file_count++;
                    }
                }
                break;
            }
        }
    }

    return 0;
}

int main(int argc, char * argv[]) {

    if (argc == 1) {
        printf("\n[ =tuneclip= ]\n");
        printf("A simple (free and open source) music player for simple people.\n");
        printf("Usage:\n");
        printf("  tuneclip <path>\n");
        printf("Path can be either be a file or a directory.\n\n");
        exit(1);
    }

    dp = init_display_struct();

    file_list = malloc(MAX_FILES * sizeof(void *));
    for (int i = 0; i < MAX_FILES; i++) {
        file_list[i] = malloc(PATH_MAX * sizeof(char));
    }

    if (ftw(argv[1], store_file, 12) == -1) { // read file list
        switch (errno) {
            case ENOENT:
                printf("Path does not exist!\n");
                break;
            case EACCES:
                printf("Insufficient permission to access path!\n");
                break;
            default:
                printf("An unknown error occurred in ftw!\n");
                break;
        }
        exit(1);
    }

    shuffle((void **) file_list, file_count);

    /* initialize ncurses interface */
    pthread_create(&interface_thread, NULL, start_interface, NULL);
    /* initialize mpv player */
    pthread_create(&player_thread, NULL, start_player, NULL);

    pthread_mutex_lock(&player_init_mutex);
    while (player_init == false) {
        pthread_cond_wait(&player_init_cond, &player_init_mutex);
    }
    pthread_mutex_unlock(&player_init_mutex);

    while (1) {
        for (int i = 0; i < file_count; i++) {
            do {
                play(file_list[i]);
            } while (dp->is_loop);
        }
    }

    pthread_join(interface_thread, NULL);
    pthread_join(player_thread, NULL);

    error_exit("End of program. (How did we get here?)");
}

