/* tuneclip
*    Copyright (C) 2022  Lucas Santos
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __GUI_CURSES_H__
#define __GUI_CURSES_H__

#include <ncurses.h>

/* macros to translate
 * key names to integers */
#define CTRL_C 3
#define LETTER_Q 113
#define LETTER_M 109
#define LETTER_L 108
#define LEFT_BRACKET 91
#define RIGHT_BRACKET 93
#define KEY_SPACEBAR 32
#define KEY_ENTER_CENTRAL 10

void * start_interface(void *data);

#endif
