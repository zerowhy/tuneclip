/* tuneclip
*    Copyright (C) 2022  Lucas Santos
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "main.h"
#include "player.h"
#include "gui_curses.h"

extern struct display * dp;
extern pthread_mutex_t locale_mutex;
extern pthread_mutex_t dp_mutex;

int max_x;
int max_y;

void format_time(int64_t time, int64_t endpos, wchar_t *buff) {
    int hour = time / 3600;
    int min = (time % 3600)/60;
    int sec = time % 60;

    int endhour = endpos / 3600;
    int endmin = (endpos % 3600)/60;
    int endsec = endpos % 60;

    if (hour < 0 || min < 0 || sec < 0)
        hour = min = sec = 0;
    else if (hour*360 + min*60 + sec > endhour*360 + endmin*60 + endsec) {
        hour = endhour;
        min = endmin;
        sec = endsec;
    }

    swprintf(buff, 128, L"%s %02d:%02d:%02d / %02d:%02d:%02d %s",
            dp->is_paused ? "⏸" : " ", hour, min, sec, endhour, endmin, endsec, dp->is_paused ? "⏸" : " ");
}

void clrln(int line) {
    move(line, 0); // move to beggining of line
    clrtoeol(); // clear line
}

void print_middle(wchar_t text[], int line) {
    int length = wcswidth(text, wcslen(text));
    clrln(line);
    mvaddwstr(line, (max_x/2) - (length/2), text);
}

void terminate_interface() {
    endwin();
    pthread_exit(NULL);
}

void update_interface(struct display * dp) {
    wchar_t * buff = malloc(CHAR_LIMIT_NAME * sizeof(wchar_t));

    swprintf(buff, CHAR_LIMIT_NAME, L"[ = tuneclip = ]");
    print_middle(buff, 0);


    pthread_mutex_lock(&dp_mutex);
    print_middle(dp->name, 1);

    format_time(dp->position_secs, dp->length_secs, buff);
    print_middle(buff, 2);

    swprintf(buff, CHAR_LIMIT_NAME, L"%s %d%%", dp->is_muted ? "🔇" : "🔊", dp->volume);
    print_middle(buff, 3);

    swprintf(buff, CHAR_LIMIT_NAME, L"%s", dp->is_loop ? "Looping" : "Not Looping");
    pthread_mutex_unlock(&dp_mutex);
    print_middle(buff, 4);


    if (max_x >= 75 && max_y >= 16) {
        mvaddwstr(7, 15, L"\n\n\n\n\n\n\n");
        mvaddwstr(8, (max_x/2)-33, L"d888888b db    db d8b   db d88888b  .o88b. db      d888888b d8888b.\n");
        mvaddwstr(9, (max_x/2)-33, L"`~~88~~' 88    88 888o  88 88'     d8P  Y8 88        `88'   88  `8D\n");
        mvaddwstr(10, (max_x/2)-33, L"   88    88    88 88V8o 88 88ooooo 8P      88         88    88oodD'\n");
        mvaddwstr(11, (max_x/2)-33, L"   88    88    88 88 V8o88 88~~~~~ 8b      88         88    88~~~  \n");
        mvaddwstr(12, (max_x/2)-33, L"   88    88b  d88 88  V888 88.     Y8b  d8 88booo.   .88.   88     \n");
        mvaddwstr(13, (max_x/2)-33, L"   YP    ~Y8888P' VP   V8P Y88888P  `Y88P' Y88888P Y888888P 88     \n");
    } else {
        mvaddwstr(7, 0, L"\n\n\n\n\n\n\n");
    }

    free(buff);
}

void * start_interface(void *data) {
    int key;

    pthread_mutex_lock(&locale_mutex);
    setlocale(LC_ALL, ""); // change locale to initialize curses screen
    initscr(); // initialize screen
    setlocale(LC_NUMERIC, "C");
    pthread_mutex_unlock(&locale_mutex);
    raw(); // disables line buffering
    noecho(); // disables echoing of user input
    curs_set(0); // hide cursor
    keypad(stdscr, TRUE); // enable reading of function keys, arrow keys and more
    timeout(200); // getch() timeout

    while (1) {
        getmaxyx(stdscr, max_y, max_x); // get screen size
        update_interface(dp);
        key = getch();
        switch (key) {
            case CTRL_C: case LETTER_Q:
                end_exit();
                break;
            case LETTER_M:
                toggle_mute();
                break;
            case LEFT_BRACKET:
                step_volume(5);
                break;
            case RIGHT_BRACKET:
                step_volume(-5);
                break;
            case KEY_RIGHT:
                seek(5);
                break;
            case KEY_LEFT:
                seek(-5);
                break;
            case KEY_UP:
                seek(60);
                break;
            case KEY_DOWN:
                seek(-60);
                break;
            case KEY_ENTER_CENTRAL:
                seek(dp->length_secs+30);
                break;
            case LETTER_L:
                /* toggles music looping */
                if (!dp->is_loop) {
                    dp->is_loop = true;
                }
                else {
                    dp->is_loop = false;
                }
                break;
            case KEY_SPACEBAR:
                toggle_pause();
                break;
        }
    }

    return NULL;
}


