default:
	gcc \
	src/main.c \
	src/gui_curses.c \
	src/player.c \
	src/czerolib/common/common.c \
	src/czerolib/queue/queue.c \
	-lpthread -lncursesw -lmpv \
	-Wall \
	-o tuneclip

debug:
	gcc \
	src/main.c \
	src/gui_curses.c \
	src/player.c \
	src/czerolib/common/common.c \
	src/czerolib/queue/queue.c \
	-lpthread -lncursesw -lmpv \
	-Wall -g \
	-o tuneclip

