# tuneclip

tuneclip is a command line application intended to be a simple music player, without flashy features.

To use it, simply run the executable and pass the file/folder you want to play:

```bash
tuneclip <file/folder>
```
